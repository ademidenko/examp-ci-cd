---

variables:
  CONTAINER_NAME: "${CI_REGISTRY}/ademidenko/examp-ci-cd"
  DOCKERFILE_PATH: ".build/Dockerfile"
  KUBERNETES_DEPLOY_NAMESPACE: "develop"

stages:
  - build
  - release
  - migrate
  - deploy

image_build:
  stage: build
  tags:
    - docker
  before_script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin
  script:
    - docker build -t "${CONTAINER_NAME}:${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}" . -f "${DOCKERFILE_PATH}"
    - docker push "${CONTAINER_NAME}:${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}"

image_release:
  stage: release
  tags: 
    - docker
  before_script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin
  script:
    - docker pull "${CONTAINER_NAME}:${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}"
    - docker tag "${CONTAINER_NAME}:${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}" "${CI_REGISTRY}/${CONTAINER_NAME}:latest"
    - docker push "${CONTAINER_NAME}:latest"

migrate:
  stage: migrate
  tags:
    - docker
  before_script:
    - apk add --no-cache curl
    - curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    - chmod +x ./kubectl
    - mv ./kubectl /bin/kubectl
    - kubectl config get-contexts
    - echo "${KUBERNETES_CERTIFICATE_AUTHORITY}" | base64 -d > /root/cert-auth
    - echo "${KUBERNETES_CLIENT_CERTIFICATE}" | base64 -d > /root/user-cert
    - echo "${KUBERNETES_CLIENT_KEY}" | base64 -d > /root/user-key
    - >
        kubectl config set-credentials "${KUBERNETES_USER}"
        --client-certificate /root/user-cert
        --client-key /root/user-key
    - >
        kubectl config set-cluster "${KUBERNETES_CLUSTER}" 
        --server="${KUBERNETES_SERVER_HOST}"
        --certificate-authority /root/cert-auth
    - > 
        kubectl config set-context gitlab-develop
        --cluster="${KUBERNETES_CLUSTER}" 
        --user="${KUBERNETES_USER}" 
    - kubectl config use-context gitlab-develop

  script:
    - kubectl apply -f .kube/migrate-job.yaml -n "${KUBERNETES_DEPLOY_NAMESPACE}"
    - while [[ $(kubectl get pods -n "${KUBERNETES_DEPLOY_NAMESPACE}" | grep 'migrate*' | awk '{print $3}') != "Completed" ]]; do echo 'wait migration...'; sleep 5; done

deploy:
  stage: deploy
  tags:
    - docker
  before_script:
    - apk add --no-cache curl
    - curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    - chmod +x ./kubectl
    - mv ./kubectl /bin/kubectl
    - kubectl config get-contexts
    - echo "${KUBERNETES_CERTIFICATE_AUTHORITY}" | base64 -d > /root/cert-auth
    - echo "${KUBERNETES_CLIENT_CERTIFICATE}" | base64 -d > /root/user-cert
    - echo "${KUBERNETES_CLIENT_KEY}" | base64 -d > /root/user-key
    - >
        kubectl config set-credentials "${KUBERNETES_USER}"
        --client-certificate /root/user-cert
        --client-key /root/user-key
    - >
        kubectl config set-cluster "${KUBERNETES_CLUSTER}" 
        --server="${KUBERNETES_SERVER_HOST}"
        --certificate-authority /root/cert-auth
    - > 
        kubectl config set-context gitlab-develop
        --cluster="${KUBERNETES_CLUSTER}" 
        --user="${KUBERNETES_USER}" 
    - kubectl config use-context gitlab-develop

  script:
    - echo "${CI_REGISTRY_CONFIG}" | base64 -d > /root/docker.json
    - >
        sed -e "s~{{ENVIRONMENT}}~${KUBERNETES_DEPLOY_NAMESPACE}~g" 
        -e "s~{{DEPLOYMENT_IMAGE}}~${CONTAINER_NAME}:${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}~g"
        .kube/application.yaml | kubectl apply -n "${KUBERNETES_DEPLOY_NAMESPACE}" -f - 
